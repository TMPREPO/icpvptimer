package mc.icej.Tasks;

import mc.icej.Main.ICPVPTimer;
import mc.icej.Managers.ChatManager;
import mc.icej.Managers.ConfigManager;
import mc.icej.Managers.PlayerDataManager;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Map;
import java.util.UUID;

public class CountdownTask extends BukkitRunnable
{
    private ICPVPTimer getPlugin = ICPVPTimer.getPlugin();
    private ConfigManager getCfg = ICPVPTimer.getConfigManager();
    private ChatManager getChatManager = ICPVPTimer.getChatManager();
    private PlayerDataManager getPD = ICPVPTimer.getPlayerDataManager();

    public void run()
    {
        for (Map.Entry<UUID, Integer> players : getPlugin.players.entrySet()) {

            players.setValue(players.getValue() - 1);

            if (players.getValue() == 0) {
                getChatManager.sendMessageToPlayer(Bukkit.getPlayer(players.getKey()), getCfg.getTimeRanOutMessage());
                getPD.setProtectedTime(Bukkit.getPlayer(players.getKey()), 0);
                getPlugin.players.remove(players.getKey());
            }
        }
    }
}
