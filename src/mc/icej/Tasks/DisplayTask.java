package mc.icej.Tasks;

import mc.icej.Main.ICPVPTimer;
import mc.icej.Managers.ChatManager;
import mc.icej.Managers.ConfigManager;
import mc.icej.Managers.FileManager;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Map;
import java.util.UUID;

public class DisplayTask extends BukkitRunnable
{
    private ICPVPTimer getPlugin = ICPVPTimer.getPlugin();
    private ChatManager getChatManager = ICPVPTimer.getChatManager();
    private ConfigManager getCfg = ICPVPTimer.getConfigManager();
    private FileManager getFileManager = ICPVPTimer.getFileManager();


    public void run()
    {
        for (Map.Entry<UUID, Integer> players : getPlugin.players.entrySet()) {
            int totalSeconds = players.getValue();
            int hours = totalSeconds / 3600;
            int minutes = (totalSeconds % 3600) / 60;
            int seconds = totalSeconds % 60;

            getChatManager.sendActionbarMessage(Bukkit.getPlayer(players.getKey()), getCfg.getShowTimeInActionBarMessage()
                    .replace("<hours>", String.valueOf(hours))
                    .replace("<minutes>", String.valueOf(minutes))
                    .replace("<seconds>", String.valueOf(seconds)));
        }
    }
}
