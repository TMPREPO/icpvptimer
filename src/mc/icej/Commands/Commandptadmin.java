package mc.icej.Commands;

import mc.icej.Main.ICPVPTimer;
import mc.icej.Managers.ChatManager;
import mc.icej.Managers.ConfigManager;
import mc.icej.Managers.FileManager;
import mc.icej.Managers.PlayerDataManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Commandptadmin implements CommandExecutor
{
    private ICPVPTimer getPlugin = ICPVPTimer.getPlugin();
    private ChatManager getChatManager = ICPVPTimer.getChatManager();
    private ConfigManager getCfg = ICPVPTimer.getConfigManager();
    private PlayerDataManager getPD = ICPVPTimer.getPlayerDataManager();
    private FileManager getFileManager = ICPVPTimer.getFileManager();
    private int totalSeconds;

    public boolean onCommand(CommandSender s, Command cmd, String str, String[] args)
    {
        if (!cmd.getName().equalsIgnoreCase("ptadmin")) {
            return true;
        }

        if (!s.hasPermission("icpvptimer.admin")) {
            getChatManager.sendMessageToSender(s, getCfg.getNoPermissionMessage());
            return true;
        }

        if (args.length == 0) {
            for (String helpmessage : getCfg.getAdminHelpPage()) {
                getChatManager.sendMessageToSender(s, helpmessage);
            }
            return true;
        }

        if (args[0].equalsIgnoreCase("settime")) {
            Integer time;
            Player target;

            if (args.length != 3) {
                getChatManager.sendMessageToSender(s, getCfg.getArgsNotFoundErrorMessage());
                return true;
            }

            target = Bukkit.getPlayerExact(args[1]);

            if (!(target instanceof Player)) {
                getChatManager.sendMessageToSender(s, getCfg.getArgsNotAPlayerErrorMessage());
                return true;
            }

            try {
                time = Integer.valueOf(args[2]);
            } catch (NumberFormatException ArgsNotANumber) {
                getChatManager.sendMessageToSender(s, getCfg.getArgsNotANumberErrorMessage());
                return true;
            }

            getPD.setProtectedTime(target, time);
            totalSeconds = time;
            int hours = totalSeconds / 3600;
            int minutes = (totalSeconds % 3600) / 60;
            int seconds = totalSeconds % 60;
            getChatManager.sendMessageToSender(s, getCfg.getProtectedTimeUpdatedMessage()
                            .replace("<hours>", String.valueOf(hours))
                            .replace("<minutes>", String.valueOf(minutes))
                            .replace("<seconds>", String.valueOf(seconds))
                            .replace("<playername>", target.getName()));
        }
        return true;
    }
}
