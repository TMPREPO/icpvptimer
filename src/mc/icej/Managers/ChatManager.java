package mc.icej.Managers;

import mc.icej.Main.ICPVPTimer;
import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class ChatManager
{
    private ICPVPTimer getPlugin = ICPVPTimer.getPlugin();
    private ConfigManager getCfg = ICPVPTimer.getConfigManager();

    public void sendMessageToPlayer(Player player, String message)
    {
        try {
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', message.replace("<prefix>", getCfg.getPrefix())));
        } catch (NullPointerException playerNotFound) {
            playerNotFound.printStackTrace();
        }
    }

    public void sendMessageToSender(CommandSender sender, String message)
    {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message.replace("<prefix>", getCfg.getPrefix())));
    }

    public void sendActionbarMessage(Player player, String message)
    {
        try
        {
            CraftPlayer p = (CraftPlayer)player;
            IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + ChatColor.translateAlternateColorCodes('&', message.replace("<prefix>", getCfg.getPrefix()) + "\"}"));
            PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte)2);
            p.getHandle().playerConnection.sendPacket(ppoc);
        } catch (NullPointerException noPlayerFound) {
        }
    }
}
