package mc.icej.Managers;

import mc.icej.Main.ICPVPTimer;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class FileManager
{
    private ICPVPTimer getPlugin = ICPVPTimer.getPlugin();
    private File dataf;
    private YamlConfiguration ymldata;


    private void createDataF()
    {
        dataf = new File(getPlugin.getDataFolder() + File.separator + "data.yml");
        ymldata = YamlConfiguration.loadConfiguration(dataf);

        if (!dataf.exists()) {
            try {
                dataf.createNewFile();
                getPlugin.log("Created data.yml.");
            } catch (IOException failureToCreateDataFile) {
                getPlugin.log("Failed to create data.yml, caused by: " + failureToCreateDataFile.getMessage());
            }
        }
    }

    public YamlConfiguration getDataF()
    {
        return ymldata;
    }

    public void saveDataF()
    {
        try {
            ymldata.save(dataf);
        } catch (IOException failedToSaveDataFile) {
            failedToSaveDataFile.printStackTrace();
        }
    }

    public void createFiles()
    {
        getPlugin.getConfig().options().copyDefaults(true);
        getPlugin.saveDefaultConfig();
        createDataF();
    }
}