package mc.icej.Managers;

import mc.icej.Main.ICPVPTimer;
import org.bukkit.entity.Player;

public class PlayerDataManager
{
    private ICPVPTimer getPlugin = ICPVPTimer.getPlugin();
    private FileManager getFileManager = ICPVPTimer.getFileManager();

    public void addPlayerToFile(Player player)
    {
        if (!getFileManager.getDataF().contains(player.getUniqueId().toString())) {
            getFileManager.getDataF().set(player.getUniqueId().toString(), getPlugin.getConfig().getInt("TimeProtected"));
            getFileManager.saveDataF();
            getPlugin.log("Added" + player.getUniqueId().toString() + "/" + player.getName() + " to data.yml");
        }
    }

    public void addPlayerToHashMap(Player player)
    {
        getPlugin.players.put(player.getUniqueId(), getFileManager.getDataF().getInt(player.getUniqueId().toString()));
    }

    public void removePlayerFromHashMap(Player player)
    {
        getFileManager.getDataF().set(player.getUniqueId().toString(), getPlugin.players.get(player.getUniqueId()));
        getFileManager.saveDataF();
        getPlugin.players.remove(player.getUniqueId());
    }

    public void setProtectedTime(Player player, Integer integer)
    {
        if (player.isOnline()) {
            getPlugin.players.put(player.getUniqueId(), integer);
        } else {
            getFileManager.getDataF().set(player.getUniqueId().toString(), integer);
            getFileManager.saveDataF();
        }
    }
}
