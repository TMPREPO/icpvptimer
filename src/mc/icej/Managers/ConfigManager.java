package mc.icej.Managers;

import mc.icej.Main.ICPVPTimer;

import java.util.List;

public class ConfigManager
{
    private ICPVPTimer getPlugin = ICPVPTimer.getPlugin();
    private FileManager getFileManager = ICPVPTimer.getFileManager();

    public String getPrefix()
    {
        return getPlugin.getConfig().getString("Messages.Prefix");
    }

    public String getNoPermissionMessage()
    {
        return getPlugin.getConfig().getString("Messages.NoPermission");
    }

    public String getTimeRanOutMessage()
    {
        return getPlugin.getConfig().getString("Messages.TimeRanOutMessage");
    }

    public List<String> getAdminHelpPage()
    {
        return getPlugin.getConfig().getStringList("Messages.AdminHelpPage");
    }

    public String getTimeLeftOnJoinNotification()
    {
        return getPlugin.getConfig().getString("Messages.OnJoinTimeLeftNotification");
    }

    public String getArgsNotANumberErrorMessage()
    {
        return getPlugin.getConfig().getString("Messages.ArgsNotANumberError");
    }

    public String getArgsNotAPlayerErrorMessage()
    {
        return getPlugin.getConfig().getString("Messages.ArgsNotAPlayerError");
    }

    public String getArgsNotFoundErrorMessage()
    {
        return getPlugin.getConfig().getString("Messages.ArgsNotFound");
    }

    public String getShowTimeInActionBarMessage()
    {
        return getPlugin.getConfig().getString("Messages.ActionBarShowTimeMessage");
    }

    public String getProtectedTimeUpdatedMessage()
    {
        return getPlugin.getConfig().getString("Messages.ProtectedTimeUpdated");
    }
}
