package mc.icej.Main;

import mc.icej.Commands.Commandptadmin;
import mc.icej.Events.*;
import mc.icej.Managers.ChatManager;
import mc.icej.Managers.ConfigManager;
import mc.icej.Managers.FileManager;
import mc.icej.Managers.PlayerDataManager;
import mc.icej.Tasks.CountdownTask;
import mc.icej.Tasks.DisplayTask;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ICPVPTimer extends JavaPlugin
{
    private static ICPVPTimer maininstance;
    private static FileManager fileinstance;
    private static ConfigManager configinstance;
    private static PlayerDataManager playerdatainstance;
    private static ChatManager chatinstance;
    private static CountdownTask countdowntask;
    private static DisplayTask displaytask;
    public HashMap<UUID, Integer> players = new HashMap<UUID, Integer>();
    public PluginManager pm = getServer().getPluginManager();

    @Override
    public void onEnable()
    {
        loadInstances();
        loadListeners();
        loadTasks();
        getCommand("ptadmin").setExecutor(new Commandptadmin());
        log("Is now enabled.");
    }

    @Override
    public void onDisable()
    {
        getCountdownTask().cancel();
        getDisplayTask().cancel();

        for (Map.Entry<UUID, Integer> playerdata : players.entrySet()) {
            getFileManager().getDataF().set(playerdata.getKey().toString(), playerdata.getValue());
            getFileManager().saveDataF();
        }

        log("Is now disabled.");
    }

    public void log(String message)
    {
        System.out.println("[ICPVPTimer] " + message);
    }

    private void loadInstances()
    {
        maininstance = this;
        fileinstance = new FileManager();
        getFileManager().createFiles();
        configinstance = new ConfigManager();
        playerdatainstance = new PlayerDataManager();
        chatinstance = new ChatManager();
        countdowntask = new CountdownTask();
        displaytask = new DisplayTask();
    }

    public void loadListeners()
    {
        pm.registerEvents(new EOnPlayerJoin(), this);
        pm.registerEvents(new EOnPlayerQuit(), this);
        pm.registerEvents(new EEntityDamageEvent(), this);
        pm.registerEvents(new EPlayerDamageByEntity(), this);
        pm.registerEvents(new EPlayerTargetedByMob(), this);
    }

    private void loadTasks()
    {
        getCountdownTask().runTaskTimerAsynchronously(this, 0, 20);
        getDisplayTask().runTaskTimerAsynchronously(this, 0, 20);
    }

    public static ICPVPTimer getPlugin()
    {
        return maininstance;
    }

    public static FileManager getFileManager()
    {
        return fileinstance;
    }

    public static ConfigManager getConfigManager()
    {
        return configinstance;
    }

    public static PlayerDataManager getPlayerDataManager()
    {
        return playerdatainstance;
    }

    public static ChatManager getChatManager()
    {
        return chatinstance;
    }

    private static CountdownTask getCountdownTask()
    {
        return countdowntask;
    }

    public static DisplayTask getDisplayTask()
    {
        return displaytask;
    }
}