package mc.icej.Events;

import mc.icej.Main.ICPVPTimer;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityTargetEvent;

public class EPlayerTargetedByMob implements Listener
{
    private ICPVPTimer getPlugin  = ICPVPTimer.getPlugin();

    @EventHandler
    public void onPlayerTargetedByMob(EntityTargetEvent e)
    {
        if (!(e.getTarget() instanceof Player)) {
            return;
        }

        Player p = (Player)e.getTarget();

        if (!getPlugin.players.containsKey(p.getUniqueId())) {
            return;
        }

        if (!(e.getEntity() instanceof Monster)) {
            return;
        }

        e.setTarget(null);
        e.setCancelled(true);
    }
}
