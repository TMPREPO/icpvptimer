package mc.icej.Events;

import mc.icej.Main.ICPVPTimer;
import mc.icej.Managers.ChatManager;
import mc.icej.Managers.ConfigManager;
import mc.icej.Managers.FileManager;
import mc.icej.Managers.PlayerDataManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class EOnPlayerJoin implements Listener
{
    private ICPVPTimer getPlugin = ICPVPTimer.getPlugin();
    private PlayerDataManager getPD = ICPVPTimer.getPlayerDataManager();
    private FileManager getFileManager = ICPVPTimer.getFileManager();
    private ChatManager getChatManager = ICPVPTimer.getChatManager();
    private ConfigManager getCfg = ICPVPTimer.getConfigManager();

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e)
    {
        Player p = e.getPlayer();

        if (!p.hasPlayedBefore()) {
            getPD.addPlayerToFile(p);
            getPD.addPlayerToHashMap(p);
            getChatManager.sendMessageToPlayer(p, getCfg.getTimeLeftOnJoinNotification());
        } else {
            if (getFileManager.getDataF().getInt(p.getUniqueId().toString()) != 0) {
                getPD.addPlayerToHashMap(p);
                int totalSeconds = getPlugin.players.get(p.getUniqueId());
                int hours = totalSeconds / 3600;
                int minutes = (totalSeconds % 3600) / 60;
                int seconds = totalSeconds % 60;
                getChatManager.sendMessageToPlayer(p, getCfg.getTimeLeftOnJoinNotification()
                        .replace("<hours>", String.valueOf(hours))
                        .replace("<minutes>", String.valueOf(minutes))
                        .replace("<seconds>", String.valueOf(seconds)));
            }
        }
    }
}
