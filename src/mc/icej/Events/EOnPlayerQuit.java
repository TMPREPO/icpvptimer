package mc.icej.Events;


import mc.icej.Main.ICPVPTimer;
import mc.icej.Managers.PlayerDataManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class EOnPlayerQuit implements Listener
{
    private ICPVPTimer getPlugin = ICPVPTimer.getPlugin();
    private PlayerDataManager getPD = ICPVPTimer.getPlayerDataManager();


    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e)
    {
        Player p = e.getPlayer();

        if (getPlugin.players.containsKey(p.getUniqueId())) {
            getPD.removePlayerFromHashMap(p);
        }
    }
}
