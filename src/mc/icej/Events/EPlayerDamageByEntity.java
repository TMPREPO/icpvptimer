package mc.icej.Events;

import mc.icej.Main.ICPVPTimer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class EPlayerDamageByEntity implements Listener
{
    private ICPVPTimer getPlugin = ICPVPTimer.getPlugin();

    @EventHandler
    public void onPlayerDamageByEntity(EntityDamageByEntityEvent e)
    {
        if (!(e.getEntity() instanceof Player)) {
            return;
        }

        Player p = (Player)e.getEntity();

        if (!getPlugin.players.containsKey(p.getUniqueId())) {
            return;
        }

        e.setCancelled(true);
    }
}
